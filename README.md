![game_name.png](https://bitbucket.org/repo/ngERbAG/images/300376400-game_name.png)

# CELTRA's programming challenge 2017 | HTML5, JS game #

The game is hosted here: [Link](http://celtra.domus-properties.com)

The documentation in pdf can be downloaded here: [celtra_doc.pdf](http://celtra.domus-properties.com/celtra_doc.pdf)